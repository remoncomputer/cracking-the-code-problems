


class Node:
    r'''Represents a Linked List Node.

        Args:
            data (int): The Data inside the linked list node.
    '''
    def __init__(self, data: int):
        self.data = data
        self.next = None

    def add_next(self, next_node: 'Node'):
        self.next = next_node
    
    def __repr__(self):
        return f'([{self.data}])'

class LinkedList:
    r'''Represents a linked list.

        Args:
            None.
    '''
    def __init__(self):
        self.root_node = None
        self.tail_node = None

    def add_node(self, data: int) -> None:
        r'''Adds a data Node to the end of list.

            Args:
                data: The data to be added to the end of the list.
        '''
        if self.tail_node == None:
            self.tail_node = Node(data)
            self.root_node = self.tail_node
        else:
            last_node = Node(data)
            self.tail_node.add_next(last_node)
            self.tail_node = last_node
    
    def remove_node(self, data: int) -> None:
        r'''Removes a node that match the data or do nothing if no match found.

            Args:
                data (int): The data to be matched.

            Returns:
                None
        '''
        if self.root_node is None:
            return
        if self.root_node.data == data:
            if self.root_node == self.tail_node:
                self.root_node = None
                self.tail_node = None
            else:
                self.root_node = self.root_node.next
            return
        current_node = self.root_node
        while current_node.next is not None:
            if current_node.next.data == data:
                current_node.next = current_node.next.next
                if current_node.next is None:
                    self.tail_node = current_node
                return
            current_node = current_node.next
        return

    def __repr__(self):
        if self.root_node is None:
            return '[Empty List]'
        nodes_repr = []
        current_node = self.root_node
        while current_node is not None:
            nodes_repr.append(str(current_node))
            current_node = current_node.next
        nodes_str = '->'.join(nodes_repr)
        linked_repre = f'[{nodes_str}]'
        return linked_repre


if __name__ == '__main__':
    l = LinkedList()
    l.add_node(1)
    l.add_node(2)
    l.add_node(3)
    l.add_node(4)
    print('l:', str(l))
    l.remove_node(3)
    print('l:', str(l))
    l.remove_node(2)
    print('l:', str(l))