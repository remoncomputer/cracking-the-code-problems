


class Node:
    r'''Represents a Linked List Node.

        Args:
            data (int): The Data inside the linked list node.
    '''
    def __init__(self, data: int):
        self.data = data
        self.next = None

    def add_next(self, next_node: 'Node'):
        self.next = next_node
    
    def __repr__(self):
        return f'([{self.data}])'

class LinkedList:
    r'''Represents a linked list.

        Args:
            None.
    '''
    def __init__(self):
        self.root_node = None
        self.tail_node = None

    def add_node(self, data: int) -> None:
        r'''Adds a data Node to the end of list.

            Args:
                data: The data to be added to the end of the list.
        '''
        if self.tail_node == None:
            self.tail_node = Node(data)
            self.root_node = self.tail_node
        else:
            last_node = Node(data)
            self.tail_node.add_next(last_node)
            self.tail_node = last_node
    
    def remove_node(self, data: int) -> None:
        r'''Removes a node that match the data or do nothing if no match found.

            Args:
                data (int): The data to be matched.

            Returns:
                None
        '''
        if self.root_node is None:
            return
        if self.root_node.data == data:
            if self.root_node == self.tail_node:
                self.root_node = None
                self.tail_node = None
            else:
                self.root_node = self.root_node.next
            return
        current_node = self.root_node
        while current_node.next is not None:
            if current_node.next.data == data:
                current_node.next = current_node.next.next
                if current_node.next is None:
                    self.tail_node = current_node
                return
            current_node = current_node.next
        return

    def __repr__(self):
        if self.root_node is None:
            return '[Empty List]'
        nodes_repr = []
        current_node = self.root_node
        while current_node is not None:
            nodes_repr.append(str(current_node))
            current_node = current_node.next
        nodes_str = '->'.join(nodes_repr)
        linked_repre = f'[{nodes_str}]'
        return linked_repre

def remove_dups_O_of_N(linked_list: LinkedList) -> None:
    r'''Remove duplicates of linked list in O(N) space and Time using a set.

        Args:
            linked_list (LinkedList): List to remove duplicates.

        Returns:
            None.
    '''
    found_vals = set()
    current_node = linked_list.root_node
    while current_node.next != None:
        found_vals.add(current_node.data)
        if current_node.next.data in found_vals:
            # Found a duplicate value
            # Remove the next node
            current_node.next = current_node.next.next
            if current_node.next == None:
                # If we are deleting the last node 
                linked_list.tail_node = current_node
                return
        else:
            # Not a duplicate value so advance the pointer
            current_node = current_node.next


def remove_dups_O_of_N_squared(linked_list: LinkedList):
    r'''Remove duplicates in a linked list using two pointers, which is O(N^2)
        in Time and O(1) in space.

        Args:
            linked_list (LinkedList): List to remove duplicates.

        Returns:
            None.
    '''
    slow_runner = linked_list.root_node
    fast_runner = slow_runner
    while slow_runner.next != None:
        # I am assuming that the list has at least two nodes
        while fast_runner.next != None:
            if fast_runner.next.data == slow_runner.data:
                # Duplicate value found
                # Remove the duplicate node
                fast_runner.next = fast_runner.next.next
                if fast_runner.next == None:
                    # We have removed the last node
                    linked_list.tail_node = fast_runner
            else:
                # The Value is not duplicate So advance the pointer
                fast_runner = fast_runner.next
        if slow_runner.next:
            # Make sure that you can iterate threw another cycle of the linked
            # list
            # Advance the slow runner
            slow_runner = slow_runner.next
            # Reset the fast_runner to the next pointer of the slow_runner
            fast_runner = slow_runner


if __name__ == '__main__':
    l1 = LinkedList()
    l1.add_node(1)
    l1.add_node(2)
    l1.add_node(3)
    l1.add_node(2)
    l1.add_node(3)
    l1.add_node(4)
    print('l1: ', str(l1))
    remove_dups_O_of_N(l1)
    print('l1 after remove dup O(N): ', l1)
    l2 = LinkedList()
    l2.add_node(1)
    l2.add_node(2)
    l2.add_node(3)
    l2.add_node(2)
    l2.add_node(3)
    l2.add_node(4)
    print('l2: ', str(l2))
    remove_dups_O_of_N_squared(l2)
    print('l2 after remove dup O(N^2): ', l2)