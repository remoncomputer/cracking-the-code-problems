

def is_unique_with_additional_data_structure(text: str) -> bool:
    r'''The fuction determines if the text is unique or not, using addtional
        data structure which is a set.

        Args:
            text (str): The text to deterime.

        Returns:
            bool: Is the text unique or not.
    '''
    s = set()
    for ch in text:
        if ch in s:
            return False
        s.add(ch)
    return True

def is_unique_in_n_square(text: str) -> bool:
    r'''Determines if the text is unique using a bubble sort similar approach
        which would take n squared approach.

        Args:
            text (str): The text to deterime.

        Returns:
            bool: Is the text unique or not.
    '''
    for idx_i in range(len(text) - 1):
        for idx_j in range(idx_i  + 1, len(text)):
            if text[idx_i] == text[idx_j]:
                return False
    return True

if __name__ == '__main__':
    str1 = 'abcdef'
    str2 = 'abcdefa'
    print(is_unique_with_additional_data_structure(str1))
    print(is_unique_in_n_square(str1))
    print(is_unique_with_additional_data_structure(str2))
    print(is_unique_in_n_square(str2))