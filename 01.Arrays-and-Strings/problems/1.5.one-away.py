

# This Solution is buggy Review the Book Solution
def one_edit_away(text1: str, text2: str) -> bool:
    r'''Checks if two texts are one (or zero) edits away.

        Args:
            text1 (str): The first text.
            text2 (str): the second text.
            
        Returns:
            bool: True if text is one or zero edits away.
    '''
    l1 = len(text1)
    l2 = len(text2)
    # if l1 == l2:
    #     edit_found = False
    # elif abs(l1 - l2) == 1:
    #     edit_found = True
    # else:
    #     # Two letters has been inserted or removed
    #     return False
    if abs(l1 - l2) > 1:
        # More than one character insertion or removal
        return False

    edit_found = False
    
    idx1 = 0   # The first string index
    idx2 = 0   # The second string index
    while idx1 < l1 and idx2 < l2:
        if text1[idx1] == text2[idx2]:
            # The two characters are the same increment the indices
            idx1 += 1
            idx2 += 1
        else:
            if (idx1 + 1) < l1 and text1[idx1 + 1] == text2[idx2]:
                # A character has been removed of text1
                # Can this case make a bug
                idx1 += 2
                idx2 += 1
                if edit_found:
                    return False
                edit_found = True
            elif (idx2 + 1) < l2 and text1[idx1] == text2[idx2 + 1]:
                # A character has been removed of text2
                # Can this case make a bug
                idx1 += 1
                idx2 += 2
                if edit_found:
                    return False
                edit_found = True
            elif (idx1 + 1) < l1 and (idx2 + 1) < l2 and \
                text1[idx1 + 1] == text2[idx2 + 1]:
                # A Character has been replaced
                idx1 += 2
                idx2 += 2
                if edit_found:
                    return False
                edit_found = True
            else:
                # double removing
                idx1 += 1
                idx2 += 1
                if edit_found:
                    return False
                edit_found = True
    return True


