


def is_substring_after_rotation(full_text: str, sub_text: str) -> bool:
    r'''Checks if the sub_text can be found in full_text after rotation.

        Args:
             full_text (str): The Big string.
             sub_text (str): the substring.
             
        Returns:
            bool: If the the sub_text is a substring or not. 
    '''
    if len(sub_text) > len(full_text):
        return False
    
    middle_char_idx_plus_one = len(full_text) // 2
    new_rotated_text = full_text + full_text[:middle_char_idx_plus_one]
    return (sub_text in new_rotated_text)


if __name__ == '__main__':
    full_text = 'abcde'
    sub_text = 'cdea'
    print('Is Substring: ', is_substring_after_rotation(full_text, sub_text))
