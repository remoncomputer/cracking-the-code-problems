from typing import List


def is_count_character_frequency_odd(text: str) -> List[bool]:
    r'''Returns an array in which each character count is odd or not,

        ex:
            if 'a' counts is even then its index will be False.
            if 'b' counts is odd then its index would be True.

        Args:
            text (str): The input text.

        Returns:
            List[bool]: Array in which if charcter count is odd then its index
                would be True else it would be False.
    ''' 
    length_of_array = ord('z') + 1 - ord('a')
    output_bit_vector = [False] * length_of_array
    # normalize text
    text = text.lower()
    # boundary values
    min_ord = ord('a')
    max_ord = ord('z')
    for ch in text:
        ch_ord = ord(ch)
        if ch_ord >= min_ord and ch_ord <= max_ord:
            ch_idx = ch_ord - min_ord
            # Toggling the bit vector because it seems like incrementing the
            # character count
            output_bit_vector[ch_idx] = not output_bit_vector[ch_idx]
    return output_bit_vector

def is_palindorme_permutation(text: str) -> bool:
    r'''Checks if the text is palindome permutation, by checking that all the
        characters counts are even with max of one odd character.

        Args:
            text (str): The input text.

        Returns:
            bool: True if the text is palindome permutation.
    '''
    is_count_odd_bit_vector = is_count_character_frequency_odd(text)
    odd_found = False
    for bit_val in is_count_odd_bit_vector:
        if bit_val and odd_found:
            # We previous found an odd value and we found another one
            return False
        elif bit_val:
            # We found the first odd value
            odd_found = True
    return True

if __name__ == '__main__':
    text1 = 'abc ! habcabcabc'
    print('Text1 is parlindorme permutation: ',
          is_palindorme_permutation(text1))
    text2 = 'abcabcnd'
    print('Text2 is parlindorme permutation: ',
          is_palindorme_permutation(text2))