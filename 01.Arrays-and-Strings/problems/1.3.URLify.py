from typing import List, Union


def URLify(character_array: List[Union[str, int]], str_len: int) -> None:
    r'''Replaces the space character by ['%', '2', '0'], works inplace on the
        string.

        Args:
            character_array (List[Union[str, int]]): The string in the form of
                character array, the function works on this string inplace.
            str_len (int): The real character length.

        Returns:
            None
    '''
    # Counting the space symbol
    space_cnt = 0  # Count of space character
    for idx in range(str_len):
        if character_array[idx] == ' ':
            space_cnt += 1
    # Translating the charcaters and replaceing space by  ['%', '2', '0']
    space_replace = ['%', '2', '0']
    new_str_length = (str_len - space_cnt) + space_cnt * len(space_replace)
    new_idx = new_str_length - 1
    old_idx = str_len - 1
    while old_idx > 0:
        if character_array[old_idx] != ' ':
            character_array[new_idx] = character_array[old_idx]
            new_idx -= 1
            old_idx -= 1
        else:
            for idx_replace in range(len(space_replace) - 1, -1, -1):
                character_array[new_idx] = space_replace[idx_replace]
                new_idx -= 1
            old_idx -= 1


if __name__ == '__main__':
    text = 'abc 234 5 6'
    str_len = len(text)
    character_array = list(text) + [0] * 100
    URLify(character_array, str_len)
    print(character_array)