from typing import List, Tuple


def get_zero_colomns_and_rows(mat: List[List[int]]) -> \
    Tuple[List[bool], List[bool]]:
    r'''Gets two bit vectors, the fist bitvector indicates the entries of cols
        to be zeroed, the second idicates the rows.

        Args:
            mat (List[List[int]]): The matrix.
        Returns:
            Tuple[List[bool], List[bool]]: cols bit vector and rows bit vector
    '''
    n = len(mat)
    cols_bit_vector = [False] * n
    rows_bit_vector = [False] * n
    for idx_i in range(n):
        for idx_j in range(n):
            if mat[idx_i][idx_j] == 0:
                cols_bit_vector[idx_i] = True
                rows_bit_vector[idx_j] = True
    return cols_bit_vector, rows_bit_vector


def zero_matrix(mat: List[List[int]]) -> None:
    r'''Zeroing a row and a coloumn in the matrix if an element contained in 
        this row and coloumn is zero

        Args:
            mat (List[List[int]]): The matrix.

        Returns:
            None
    '''
    cols_bit_vector, rows_bit_vector = get_zero_colomns_and_rows(mat)
    n = len(mat)
    for idx_i in range(n):
        for idx_j in range(n):
            if cols_bit_vector[idx_i] or rows_bit_vector[idx_j]:
                mat[idx_i][idx_j] = 0


if __name__ == '__main__':
    mat = [[1, 2, 3], [5, 6, 7], [2, 3, 0]]
    zero_matrix(mat)
    print(mat)


