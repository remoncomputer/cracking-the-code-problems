from typing import Dict

def make_charcter_count(text: str) -> Dict[str, int]:
    r'''Returns a hash table were each entry contains the charcater with each
        count.

        Args:
            text (str): The text to check.

        Returns:
            Dict[str, int]: Each character with each count.
    ''' 
    charcater_vs_count = {}
    for ch in text:
        if ch not in charcater_vs_count:
            charcater_vs_count[ch] = 0
        charcater_vs_count[ch] += 1
    return charcater_vs_count

def check_text_permutation(text1: str, text2: str) -> bool:
    r'''Checks if text1 is a permutation of text2 using a hash table of
        characters count, this fuction take O(n) in space and time.

        Args:
            text1 (str): The first text.
            text2 (str): The second text.

        Returns:
            bool: True if the texts are permutations of one another otherwise
                false.
    '''
    hash_count_1 = make_charcter_count(text1)
    hash_count_2 = make_charcter_count(text2)
    return hash_count_1 == hash_count_2

def check_text_permutation_with_sort(text1: str, text2: str) -> bool:
    r'''Checks if text1 is a permutation of text2 using sorting, I am assuming
        that the sorting is quick sort so the time complixety is O(n log n)
        and the space complexity is O(1).

        Args:
            text1 (str): The first text.
            text2 (str): The second text.

        Returns:
            bool: True if the texts are permutations of one another otherwise
                false.
    '''
    text1 = sorted(text1)
    text2 = sorted(text2)
    return text1 == text2

if __name__ == '__main__':
    text1 = 'abc'
    text2 = 'cab'
    print(check_text_permutation(text1, text2))
    print(check_text_permutation_with_sort(text1, text2))