from typing import List


def swap_elements(mat: List[List[int]], i_first: int, j_first: int,
                  i_second: int, j_second: int, i_center: int, j_center :int) \
                      -> None:
    r'''swap the elements specefied by the (i_first, j_first) and
        (i_second, j_second)

        Args:
            mat (List[List[int]]): The matrix that contains the elements.
            i_first (int): First element i-coordinate.
            j_first (int): First element j-coordinate,
            i_second (int): Second element i-coordinate.
            j_second (int): Second element j-coordinate.
            i_center (int): Center point i-coordinate.
            j_center (int): Center point j-coordinate.
    '''
    # Perform the shift relative to center point coordinate, to get the indices
    i_first += i_center
    j_first += j_center
    i_second += i_center
    j_second += j_center
    # Swicthing the variables
    temp = mat[i_first][j_first]
    mat[i_first][j_first] = mat[i_second][j_second]
    mat[i_second][j_second] = temp


def rotate_matrix(mat: List[List[int]]) -> None:
    r'''Rotate the matrix by 90 degrees in place.

        Args:
            mat (List[List[int]]): The matrix to rotate.

        Returns:
            None.
    '''
    h_dim = len(mat)
    w_dim = len(mat[0])
    i_center = h_dim // 2
    j_center = w_dim // 2 
    delta_i = h_dim - i_center
    delta_j = w_dim - j_center
    for first_quad_i in range(0, delta_i):
        for first_quad_j in range(1, delta_j):
            forth_quad_i, forth_quad_j = first_quad_j, -first_quad_i
            third_quad_i, third_quad_j = -first_quad_i, -first_quad_j
            second_quad_i, second_quad_j = -first_quad_j, first_quad_i
            swap_elements(mat, first_quad_i, first_quad_j, second_quad_i,
                          second_quad_j, i_center, j_center)
            swap_elements(mat, first_quad_i, first_quad_j, third_quad_i,
                          third_quad_j, i_center, j_center)
            swap_elements(mat, first_quad_i, first_quad_j, forth_quad_i,
                          forth_quad_j, i_center, j_center)


if __name__ == '__main__':
    mat = [[1, 0, 0, 0, 0, 0, 0],
           [0, 1, 0, 0, 0, 0, 0],
           [0, 0, 1, 0, 0, 0, 0],
           [0, 0, 0, 1, 0, 0, 0],
           [0, 0, 0, 0, 1, 0, 0],
           [0, 0, 0, 0, 0, 1, 0],
           [0, 0, 0, 0, 0, 0, 1]]
    rotate_matrix(mat)
    print('Rotated Matrix: ', mat)
