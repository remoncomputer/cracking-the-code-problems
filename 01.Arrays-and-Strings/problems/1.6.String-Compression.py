


def compress_string(text: str) -> str:
    r'''Perform String Compression, if the compressed string length is greater
        than the original string then return the original string.

        ex:
            'aaabbcc' --> 'a3b2c2'

        Args:
            text (str): The text to be compressed.

        Returns:
            str: The compressed string or the original one
    '''
    compressed_char_list = []
    last_char = text[0]
    last_char_repetition = 1
    compressed_char_list.append(last_char)
    for ch in text[1:]:
        if ch == last_char:
            last_char_repetition += 1
        else:
            # If a new character appeared
            compressed_char_list.append(str(last_char_repetition))
            last_char_repetition = 1
            last_char = ch
            compressed_char_list.append(last_char)
    # Append the repetition of the last char
    compressed_char_list.append(str(last_char_repetition))
    if len(compressed_char_list) > len(text):
        return text
    else:
        compressed_str = ''.join(compressed_char_list)
        return compressed_str

if __name__ == '__main__':
    text = 'aaabbbccc'
    print('Compressed Text: ', compress_string(text))
