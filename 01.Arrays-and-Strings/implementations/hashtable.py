from typing import Union
from functools import reduce


class KeyValueNode:
    r'''Represents a key value node in a linked list.

        Args:
            key (str): The key value.
            value (object): The value element.

    '''
    def __init__(self, key: str, value: object):
        self.key = key
        self.value = value
        self.next_node = None
    
    def add_next_node(self, next_node: "KeyValueNode") -> None:
        r'''Adds the next node to the current node.

            Args:
                next_node (KeyValueNode): The next node to be added.

            Returns:
                None
        '''
        self.next_node = next_node

    def get_next_node(self) -> Union["KeyValueNode", None]:
        r'''Returns the next node of the current node.
        '''
        return self.next_node

    def __repr__(self) -> str:
        r'''Defines the string representation of the node.

            Args:
                None.

            Returns:
                str: The string representation.
        '''
        representation = f"Node('{self.key}')->{self.value}"
        return representation


def simple_string_sum_hashing(key: str) -> int:
    r'''Applies simple hashing of strings using the sum of its characters,
        after converting them to integers.

        Args:
            key (str): The key string.

        Returns:
            int: The hash code of the string
    '''
    hash_code = reduce(lambda accum, el: accum + ord(el), key, 0)
    return hash_code


class KeyValueLinkedList:
    r'''Represents a very simple Key and Value Linked List.

        Args:
            None
    '''
    def __init__(self):
        self.root_node = None
        self.last_node = None

    def __contains__(self, key: str) -> bool:
        r'''Implements the in operator in python.

            Args:
                key (str): The key to check.

            Returns:
                bool: True if the key is in the list else False
        '''
        if self.root_node is None:
            return False
        current_node = self.root_node
        while (current_node is not None):
            if current_node.key == key:
                return True
            current_node = current_node.get_next_node()
        return False

    def add_node(self, key: str, value: object) -> None:
        r'''Adds a node to the current linked list.

            Args:
                key (str): The new element key.
                value (object): The new element value.
        '''
        if self.last_node is None:
            new_node = KeyValueNode(key, value)
            self.last_node = new_node
            self.root_node = new_node
            return
        # # asserts that the element does not exist in the linked list
        # assert key not in self, f"Cannot add a repeated key: {key}"
        # # adds the node to the last node
        # self.last_node.add_next_node(new_node)
        # self.last_node = new_node
        
        # Search for the key inside the current list
        current_node = self.root_node
        while current_node is not None:
            if current_node.key == key:
                current_node.value = value
                return
            current_node = current_node.next_node
        # The key is not found so add it to the last node
        new_node = KeyValueNode(key, value)
        self.last_node.add_next_node(new_node)
        self.last_node = new_node

    def __getitem__(self, key: str) -> object:
        r'''Returns the item specefied by the certain key or throws a value
            error.

            Args:
                key (str): The key to search for.

            Returns:
                object: The object value or throws a value error.
        '''
        current_node = self.root_node
        while current_node is not None:
            if current_node.key == key:
                return current_node.value
            current_node = current_node.next_node
        raise ValueError(f'Key: {key} does not exists')

    def __repr__(self):
        r'''The String representation of the linked list.

            Args:
                None.

            Returns:
                str: The String Representation of the linked list.
        '''
        if self.root_node is None:
            return "[]"
        nodes = []
        current_node = self.root_node
        while current_node is not None:
            nodes.append(current_node)
            current_node = current_node.next_node
        representation = str(nodes)
        return representation


class SimpleHashTable:
    r'''Simple Hash table implementation with simple hash function.

        Args:
            capacity (int): The HashTable Capacity.
    '''
    def __init__(self, capacity: int):
        self.capacity = capacity
        self.array = [KeyValueLinkedList() for _ in range(capacity)]

    def key_to_bucket(self, key: str) -> KeyValueLinkedList:
        r'''Map the key to key value linked list (the bucket).

            Args:
                key (str): The key to hash.

            Returns:
                KeyValueLinkedList: The bucket in which we keep the keys along
                    with their values.
        '''
        hash_code = simple_string_sum_hashing(key)
        bucket_idx = hash_code % self.capacity
        bucket = self.array[bucket_idx]
        return bucket

    def __contains__(self, key: str) -> bool:
        r'''Implements the in operator to test if a key exists in the hashtable
            or not.

            Args:
                key (str): The key to check.

            Returns:
                bool: does the key exists in the array or not.
        '''
        bucket = self.key_to_bucket(key)
        return (key in bucket)

    def __setitem__(self, key: str, value: object) -> None:
        r'''Sets a key value pair within the hash table.

            Args:
                key (str): The key value.
                value (object): The object value.
        '''
        bucket = self.key_to_bucket(key)
        bucket.add_node(key, value)
    
    def __getitem__(self, key: str) -> object:
        r'''Returns the item specefied by the key.

            Args:
                key (str): The key value to get.

            Returns:
                object: The object value if exits or threw a value error if the
                    key does not exist in the bucket.
        '''
        bucket = self.key_to_bucket(key)
        return bucket[key]

    def __repr__(self) -> str:
        r'''Returns the string representation of the hash table.

            Args:
                None

            Returns:
                The String Representation of the object.
        '''
        representation = "[\n\t"
        representation += ',\n\t'.join([str(bucket) for bucket in self.array])
        representation += "\n]"
        return representation

if __name__ == '__main__':
    hash_table = SimpleHashTable(2)
    print('Adding hash_table[\'ab\'] = 3')
    hash_table['ab'] = 3
    print(hash_table)
    print('Adding hash_table[\'cd\'] = 4')
    hash_table['cd'] = 4
    print(hash_table)
    print('Adding hash_table[\'ef\'] = 3')
    hash_table['ef'] = 3
    print(hash_table)
    print('Adding hash_table[\'cd\'] = 9')
    hash_table['cd'] = 9
    print(hash_table)
    print('hash_table[\'cd\'] = ', hash_table['cd'])
        

