

class ArrayList:
    r'''Example ArrayList implementation

        Args:
            initial_capacity (int): The intial capacity of the array.
    '''
    def __init__(self, initial_capacity: int):
        self.array = [0] * initial_capacity
        self.size = initial_capacity
        self.last_element_idx = -1

    def resize(self) -> None:
        r'''Resizes the array from the initial capacity with a factor of 2

            Args:
                None

            Returns:
                None
        '''
        print('Resizing the linked list to: ', self.size * 2)
        # Allocate the new array
        new_array = [0] * (self.size * 2)
        # Copy the old array
        for idx in range(self.size):
            new_array[idx] = self.array[idx]
        # Delete the old array
        del self.array
        # Sets the new array
        self.array = new_array
        self.size *= 2

    def print(self) -> None:
        r'''Prints the array list.

            Args:
                None

            Returns:
                None
        '''
        print('The  ArrayList is: ', self.array[:self.last_element_idx + 1])

    def append(self, element: object) -> None:
        r'''Adds an element to the linked list.

            Args:
                element (object): The new element to be added.

            Returns:
                None
        '''
        # If the new element will be out of bounds of the array then resize it
        if (self.last_element_idx + 1) >= self.size:
            self.resize()
        # Increment last element idx
        self.last_element_idx += 1
        self.array[self.last_element_idx] = element



if __name__ == '__main__':
    print('Allocating Array List:')
    ar = ArrayList(1)
    print('Adding 2')
    ar.append(2)
    ar.print()
    print('Adding 3')
    ar.append(3)
    ar.print()
    print('Adding 4, 5')
    ar.append(4)
    ar.append(5)
    ar.print()
    print('Adding 6')
    ar.append(6)
    ar.print()
